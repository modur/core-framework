# Documentation index
## Why use Modur Framework
* Modur Framework is lightweight PHP framework used for web applications. Thanks to simple routing, utilization of containers and intuitive logging system you can build stable and reliable web application. 
## Documentation content
### [Getting started](./GETTING_STARTED.md)
* Requirements
* Installation
* Request lifecycle
* Directory structure
* Configuration
* Create your first app
### [Routing](./ROUTING.md)
* How it works
* Routing components
* API routing
### [Container](./CONTAINER.md)
* How it works
* Adding component
* Loading component
* Component configuration
### [Logging](./LOGGING.md)
* How it works
* Log adapters
* Error codes & messages
* Debugging
### [Application](./APPLICATION.md)
* ImportTrait
* SpaceShip
* Fallback
* Functions & constants
    
###### tags: `Modur Framework v1.0.0`