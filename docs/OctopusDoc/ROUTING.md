# Routing
## How it works
1. Apache `.htaccess` rewrites all requests to `index.php` where the framework catch information about request.
2. Framework send the information to the routing controller. 
3. Routing controller checks defined routes at `/routes/web.php` configuration file:
    * If found -> Call route function
    * Otherwise -> Call HTTP 404 error
## Managing routes
* Working with `/routes/web.php`
### Defining new routes 
```
<?php
    
/** @var Routes $route */
    
// Important route namespace
use OctopusCore\Route\Routes;
    
// Add new route - add(string $name, callable $function)
$route->add('phpinfo', function() {
    phpinfo();
});
    
// Now we can show phpinfo() from http://localhost/phpinfo
```
### Route & container connection
```
// ...
    
// Extend namespaces with container interface 
use OctopusCore\Container\ContainerExtendedInterface;


// Add new route with container object passed
$route->add('user', function (
    ContainerExtendedInterface $container
) {
    $user = new User();
    $user->setContainer($container);
    // ...
})
    
// ...
``` 
## API routing
* We can define API endpoints in `/routes/api.php` configuration file. 
* Default HTTP Content-Type of this pool is `application/json`.
* API endpoints are available from http://localhost/api/{name}
* Defining new routes same as before.

###### tags: `Modur Framework v1.0.0`
