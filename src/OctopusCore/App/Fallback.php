<?php

namespace OctopusCore\App;

/**
 * Class Fallback
 * @package OctopusCore\App
 */
class Fallback
{
    public function __construct(int $code, array $inject_objects = array())
    {
        http_response_code($code);
        $file = DIR_RES . "fallback/$code.php";
        if (file_exists($file)) {
            (new SpaceShip())->fly($file, $inject_objects);
        }
    }
}