<?php

namespace OctopusCore\App;

use OctopusCore\Container\ContainerAwareTrait;
use OctopusCore\Log\ImportLoggerTrait;

trait ImportTrait
{
    use ContainerAwareTrait;
    use ImportLoggerTrait;
}