<?php

namespace OctopusCore\Route;

/**
 * Trait RouteControllerTrait
 * @package OctopusCore\Route
 */
trait RouteControllerTrait
{
    /**
     * @var RouterInterface $router
     */
    private RouterInterface $router;

    /**
     * @var array $fallbackInjects
     */
    private array $fallbackInjects;

    /**
     * RouteControllerTrait constructor.
     * @param RouterInterface $router
     * @param array $fallback_injects
     */
    public function __construct(
        RouterInterface $router,
        array $fallback_injects = array()
    ) {
        $this->router = $router;
        $this->fallbackInjects = $fallback_injects;

        $this->setRouterFallbackInjects();
    }

    /**
     * @return void
     */
    public function setRouterFallbackInjects(): void
    {
        $this->router->setFallbackInjects($this->fallbackInjects);
    }
}