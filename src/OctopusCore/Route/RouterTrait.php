<?php

namespace OctopusCore\Route;

use OctopusCore\Container\ContainerAwareTrait;

/**
 * Trait RouterTrait
 * @package OctopusCore\Route
 */
trait RouterTrait
{
    use ContainerAwareTrait {
        ContainerAwareTrait::__construct as private getContainer;
    }

    /**
     * @var array $url
     */
    private array $url;

    /**
     * @var array $params
     */
    private array $params;

    /**
     * @var array $fallbackInjects
     */
    private array $fallbackInjects;

    /**
     * RouterTrait constructor.
     */
    public function __construct()
    {
        $this->getContainer();

        $url = array_filter(explode("/", $_SERVER['REQUEST_URI']));
        foreach ($url as $pos => $value) {
            $get = explode("?", $value);
            $this->url[$pos] = $get[0];
            if (!empty($get[1])) {
                $params = explode("&", $get[1]);
                foreach ($params as $param) {
                    $param_array = explode("=", $param);
                    $this->params[$pos][$param_array[0]] = $param_array[1];
                }
            }
        }
    }

    /**
     * @param $position
     * @return string|null
     */
    public function getUrl($position): ?string
    {
        return $this->url[$position] ?? null;
    }

    /**
     * @param $position
     * @return array|null
     */
    public function getParams($position): ?array
    {
        return $this->params[$position] ?? null;
    }

    /**
     * @return int
     */
    public function getUrlCount(): int
    {
        return count($this->url);
    }

    /**
     * @param array $fallback_injects
     */
    public function setFallbackInjects(array $fallback_injects)
    {
        $this->fallbackInjects = $fallback_injects;
    }
}