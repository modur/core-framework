<?php

namespace OctopusCore\Route;

/**
 * Class Routes
 * @package OctopusCore\Route
 */
class Routes
{
    /**
     * @var array $routes
     */
    private array $routes = [];

    /**
     * @param string $name
     * @param callable $function
     */
    public function add(string $name, callable $function)
    {
        if (!isset($this->routes[$name])) {
            $this->routes[$name] = $function;
        } else {
            die("Error: The route with name $name already exists");
        }
    }

    /**
     * @return array
     */
    public function getRoutes()
    {
        return $this->routes;
    }
}