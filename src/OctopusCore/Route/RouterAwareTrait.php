<?php

namespace OctopusCore\Route;

/**
 * Trait RouterAwareTrait
 * @package OctopusCore\Route
 */
trait RouterAwareTrait
{
    /**
     * @var RouteControllerInterface|mixed|null $routeController
     */
    private ?RouteControllerInterface $routeController = null;

    /**
     * @var string|null $routeControllerClass
     */
    private ?string $routeControllerClass = null;

    /**
     * @var string|null $routerClass
     */
    private ?string $routerClass = null;

    /**
     * @var array $fallbackInjects
     */
    private array $fallbackInjects = [];

    /**
     * RouterAwareTrait constructor.
     */
    public function __construct()
    {
        $this->setRouter(NullRouter::class);
        $this->setRouteController(NullRouteController::class);

        $this->routeController = new $this->routeControllerClass(
            new $this->routerClass()
        );
    }

    /**
     * @param string $routeController
     */
    public function setRouteController(string $routeController): void {
        if (!$this->checkRouteController()) {
            if (!in_array(
                RouteControllerInterface::class,
                class_implements($routeController)
            )) {
                die("The route controller provided is not a route controller");
            } else {
                $this->routeControllerClass = $routeController;
            }
        } else {
            die("Route controller is already set");
        }
    }

    /**
     * @return bool
     */
    public function checkRouteController(): bool
    {
        if (
            !$this->routeControllerClass !== NullRouteController::class
            && $this->routeControllerClass !== null
        ) {
            return true;
        }

        return false;
    }

    /**
     * @param string $router
     */
    public function setRouter(string $router): void {
        if (!$this->checkRouter()) {
            if (!in_array(
                RouterInterface::class,
                class_implements($router)
            )) {
                die("The router provided is not a router");
            } else {
                $this->routerClass = $router;
            }
        } else {
            die("Route controller is already set");
        }
    }

    /**
     * @return bool
     */
    public function checkRouter(): bool
    {
        if (
        !$this->routerClass !== NullRouter::class
        && $this->routerClass !== null
        ) {
            return true;
        }

        return false;
    }

    /**
     * @param array $fallback_injects
     */
    public function setRouterFallbackInjects(array $fallback_injects): void
    {
        if (!empty($this->fallbackInjects)) {
            die("Fallback injects is already set");
        } else {
            $this->fallbackInjects = $fallback_injects;
        }
    }
}