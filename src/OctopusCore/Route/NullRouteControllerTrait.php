<?php

namespace OctopusCore\Route;

/**
 * Trait NullRouteControllerTrait
 * @package OctopusCore\Route
 */
trait NullRouteControllerTrait
{
    /**
     * @return void
     */
    public function run(): void
    {
        die("Error: You can´t call this method with null route controller");
    }
}