<?php

namespace OctopusCore\Route;

/**
 * Interface RouterAwareInterface
 * @package OctopusCore\Route
 */
interface RouterAwareInterface
{
    /**
     * RouterAwareInterface constructor.
     */
    public function __construct();

    /**
     * @param string $router
     */
    public function setRouter(string $router): void;

    /**
     * @param string $routeController
     */
    public function setRouteController(string $routeController): void;

    /**
     * @return bool
     */
    public function checkRouter(): bool;

    /**
     * @return bool
     */
    public function checkRouteController(): bool;

    /**
     * @param array $fallback_injects
     */
    public function setRouterFallbackInjects(array $fallback_injects): void;
}