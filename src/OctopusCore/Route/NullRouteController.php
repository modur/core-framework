<?php

namespace OctopusCore\Route;

/**
 * Class NullRouteController
 * @package OctopusCore\Route
 */
class NullRouteController implements RouteControllerInterface
{
    use RouteControllerTrait;
    use NullRouteControllerTrait;
}