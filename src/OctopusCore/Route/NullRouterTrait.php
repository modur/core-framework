<?php

namespace OctopusCore\Route;

/**
 * Trait NullRouterTrait
 * @package OctopusCore\Route
 */
trait NullRouterTrait
{
    /**
     * NullRouterTrait constructor.
     */
    public function __construct()
    {
    }

    /**
     * @param $position
     * @return string|null
     */
    public function getURL($position): ?string
    {
        die("You can´t call this method with null router");
    }

    /**
     * @param array $fallbackInjects
     */
    public function setFallbackInjects(array $fallbackInjects)
    {
        die("You can´t call this method with null router");
    }

    /**
     * @param array $routes
     * @param int $position
     */
    public function route(array $routes, int $position = 1): void
    {
        die("You can´t call this method with null router");
    }
}