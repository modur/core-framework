<?php

namespace OctopusCore\Container;

/**
 * Trait ContainerStorageAwareTrait
 * @package OctopusCore\Container
 */
trait ContainerStorageAwareTrait
{
    /**
     * @var ContainerStorageInterface $containerStorage
     */
    public ContainerStorageInterface $containerStorage;

    /**
     * ContainerStorageAwareTrait constructor.
     */
    public function __construct()
    {
        $this->setContainerStorage(new ContainerStorage());
    }

    /**
     * @param ContainerStorageInterface $storage
     */
    public function setContainerStorage(
        ContainerStorageInterface $storage
    ): void {
        if (!$this->checkContainerStorage()) {
            $this->containerStorage = $storage;
        } else {
            die("Container storage is already set");
        }
    }

    /**
     * @return bool
     */
    public function checkContainerStorage(): bool
    {
        if (!isset($this->containerStorage)) {
            return false;
        }

        return true;
    }
}