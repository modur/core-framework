<?php

namespace OctopusCore\Container;

use Exception;
use Psr\Container\NotFoundExceptionInterface;
use Throwable;

/**
 * Class NotFoundException
 * @package OctopusCore\Container
 */
class NotFoundException extends Exception implements NotFoundExceptionInterface
{
    /**
     * NotFoundException constructor.
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct(
        $message = "",
        $code = 0,
        Throwable $previous = null
    )
    {
        parent::__construct($message, $code, $previous);
    }
}