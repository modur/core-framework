<?php

namespace OctopusCore\Container;

use OctopusCore\App\SpaceShip;

/**
 * Class Container
 * @package OctopusCore\Container
 */
class Container implements ContainerExtendedInterface
{
    use ContainerTrait;

    /**
     * @param string $id
     * @return mixed
     * @throws NotFoundException
     */
    public function get($id)
    {
        if (!$this->has("app")) {
            throw new NotFoundException(
                "App component not found in container storage",
                7004
            );
        } else {
            if (!$this->has($id)) {
                throw new NotFoundException(
                    "Component with id $id not found in storage",
                    7003
                );
            } elseif (!isset($this->storage->instances[$id])) {
                $instance = new $this->storage->components[$id]();
                $storage_instance = $this->storage->instances[$id] = $instance;

                if ($storage_instance instanceof ContainerAwareInterface) {
                    $storage_instance->setContainer($this);
                }

                $file = DIR_CONF . $id . ".php";
                if (file_exists($file)) {
                    (new SpaceShip())->fly($file);
                }
            }
        }

        return $this->storage->instances[$id];
    }
}