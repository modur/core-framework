<?php

namespace OctopusCore\Container;

/**
 * Class NullContainer
 * @package OctopusCore\Container
 */
class NullContainer implements ContainerExtendedInterface
{
    use ContainerTrait;
    use NullContainerTrait;
}