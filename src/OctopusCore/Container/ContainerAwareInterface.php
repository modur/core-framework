<?php

namespace OctopusCore\Container;

/**
 * Interface ContainerAwareInterface
 * @package OctopusCore\Container
 */
interface ContainerAwareInterface
{
    /**
     * ContainerAwareInterface constructor.
     */
    public function __construct();

    /**
     * @param ContainerExtendedInterface $container
     */
    public function setContainer(ContainerExtendedInterface $container): void;

    /**
     * @return bool
     */
    public function checkContainer(): bool;
}