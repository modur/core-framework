<?php

namespace OctopusCore\Container;

/**
 * Trait ContainerBuilderAwareTrait
 * @package OctopusCore\Container
 */
trait ContainerBuilderAwareTrait
{
    use ContainerAwareTrait;

    /**
     * @var ContainerBuilderInterface|null $containerBuilder
     */
    protected ?ContainerBuilderInterface $containerBuilder = null;

    /**
     * ContainerBuilderAwareTrait constructor.
     */
    public function __construct()
    {
        $this->setContainerStorage(new ContainerStorage());
        $this->setContainer(new NullContainer($this->containerStorage));
        $this->setContainerBuilder(new NullContainerBuilder(
            $this->containerStorage
        ));
    }

    /**
     * @param ContainerBuilderInterface $builder
     */
    public function setContainerBuilder(
        ContainerBuilderInterface $builder
    ): void {
        if (!$this->checkContainerBuilder()) {
            $this->containerBuilder = $builder;
        } else {
            die("Container builder is already set");
        }
    }

    /**
     * @return bool
     */
    public function checkContainerBuilder(): bool
    {
        if (
            !($this->containerBuilder instanceof NullContainerBuilder)
            && $this->containerBuilder != null
        ) {
            return true;
        }

        return false;
    }
}