<?php

namespace OctopusCore\Container;

use Exception;
use Psr\Container\ContainerExceptionInterface;
use Throwable;

/**
 * Class ContainerException
 * @package OctopusCore\Container
 */
class ContainerException extends Exception implements
    ContainerExceptionInterface
{
    /**
     * ContainerException constructor.
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct(
        $message = "",
        $code = 0,
        Throwable $previous = null
    ) {
        parent::__construct($message, $code, $previous);
    }
}