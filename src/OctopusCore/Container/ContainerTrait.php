<?php

namespace OctopusCore\Container;

/**
 * Trait ContainerTrait
 * @package OctopusCore\Container
 */
trait ContainerTrait
{
    /**
     * @var ContainerStorageInterface $storage
     */
    private ContainerStorageInterface $storage;

    /**
     * ContainerTrait constructor.
     * @param ContainerStorageInterface $storage
     */
    public function __construct(ContainerStorageInterface $storage)
    {
        $this->storage = $storage;
    }

    /**
     * @param $id
     * @return bool
     */
    public function has($id)
    {
        return isset($this->storage->components[$id]);
    }
}