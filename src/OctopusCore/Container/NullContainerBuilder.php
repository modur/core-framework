<?php

namespace OctopusCore\Container;

/**
 * Class NullContainerBuilder
 * @package OctopusCore\Container
 */
class NullContainerBuilder implements ContainerBuilderInterface
{
    use ContainerTrait;
    use NullContainerBuilderTrait;
}