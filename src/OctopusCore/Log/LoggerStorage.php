<?php

namespace OctopusCore\Log;

use Psr\Log\LogLevel;

/**
 * Class LoggerStorage
 * @package OctopusCore\Log
 */
class LoggerStorage implements LoggerStorageInterface
{
    /**
     * Simple array to store all registered loggers
     *
     * @var array
     */
    public array $loggers = [];

    /**
     * Array which maps registered loggers to a single log level
     *
     * @var array|array[]
     */
    public array $loggersMap = [
        LogLevel::EMERGENCY => [],
        LogLevel::ALERT     => [],
        LogLevel::CRITICAL  => [],
        LogLevel::ERROR     => [],
        LogLevel::WARNING   => [],
        LogLevel::NOTICE    => [],
        LogLevel::INFO      => [],
        LogLevel::DEBUG     => []
    ];
}