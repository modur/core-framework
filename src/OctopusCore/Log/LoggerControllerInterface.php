<?php

namespace OctopusCore\Log;

/**
 * Interface LoggerControllerInterface
 * @package OctopusCore\Log
 */
interface LoggerControllerInterface
{
    /**
     * LoggerControllerInterface constructor.
     * @param LoggerStorageInterface $storage
     */
    public function __construct(LoggerStorageInterface $storage);

    /**
     * @param string $code
     * @param array $context
     * @return mixed
     */
    public function debug(string $code, array $context = array());

    /**
     * @param $level
     * @param string $code
     * @param array $context
     * @param string $logger
     * @return mixed
     */
    public function log($level, string $code, array $context = array(), string $logger = '0');
}