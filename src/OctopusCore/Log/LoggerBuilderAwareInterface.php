<?php

namespace OctopusCore\Log;

/**
 * Interface LoggerBuilderAwareInterface
 * @package OctopusCore\Log
 */
interface LoggerBuilderAwareInterface
{
    /**
     * LoggerBuilderAwareInterface constructor.
     */
    public function __construct();

    /**
     * @param LoggerBuilderInterface $builder
     */
    public function setLoggerBuilder(
        LoggerBuilderInterface $builder
    ): void;

    /**
     * @return bool
     */
    public function checkLoggerBuilder(): bool;
}