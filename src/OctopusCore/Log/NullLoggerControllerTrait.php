<?php

namespace OctopusCore\Log;

use Exception;

/**
 * Trait NullLoggerControllerTrait
 * @package OctopusCore\Log
 */
trait NullLoggerControllerTrait
{
    /**
     * @param $level
     * @param string $code
     * @param array $context
     * @param string $logger
     * @throws Exception
     */
    public function log($level, string $code, array $context = array(), string $logger = '0')
    {
        throw new Exception(
            "You can´t use the logger storage with null container",
            7033
        );
    }
}