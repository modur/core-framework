<?php

namespace OctopusCore\Log;

/**
 * Interface LoggerControllerAwareInterface
 * @package OctopusCore\Log
 */
interface LoggerControllerAwareInterface
{
    /**
     * LoggerControllerAwareInterface constructor.
     */
    public function __construct();

    /**
     * @param LoggerControllerInterface $loggerController
     */
    public function setLoggerController(
        LoggerControllerInterface $loggerController
    ): void;

    /**
     * @return bool
     */
    public function checkLoggerController(): bool;
}