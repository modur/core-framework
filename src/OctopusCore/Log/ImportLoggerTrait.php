<?php

namespace OctopusCore\Log;

use OctopusCore\Container\ContainerAwareTrait;

/**
 * Trait ImportLoggerTrait
 * @package OctopusCore\Log
 */
trait ImportLoggerTrait
{
    use ContainerAwareTrait;

    /**
     * @var LoggerControllerInterface $loggerController
     */
    private LoggerControllerInterface $loggerController;

    /**
     * @return mixed|LoggerControllerInterface
     */
    private function logger()
    {
        if (empty($this->loggerController)) {
            $this->loggerController = $this->container->get("log");
        }

        return $this->loggerController;
    }
}