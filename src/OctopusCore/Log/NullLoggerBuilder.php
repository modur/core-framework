<?php

namespace OctopusCore\Log;

/**
 * Class NullLoggerBuilder
 * @package OctopusCore\Log
 */
class NullLoggerBuilder implements LoggerBuilderInterface
{
    use LoggerStorageTrait;
    use NullLoggerBuilderTrait;
}