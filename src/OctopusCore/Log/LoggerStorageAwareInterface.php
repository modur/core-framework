<?php

namespace OctopusCore\Log;

/**
 * Interface LoggerStorageAwareInterface
 * @package OctopusCore\Log
 */
interface LoggerStorageAwareInterface
{
    /**
     * LoggerStorageAwareInterface constructor.
     */
    public function __construct();

    /**
     * @param LoggerStorageInterface $storage
     */
    public function setLoggerStorage(
        LoggerStorageInterface $storage
    ): void;

    /**
     * @return bool
     */
    public function checkLoggerStorage(): bool;
}