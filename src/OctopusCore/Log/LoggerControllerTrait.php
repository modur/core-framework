<?php

namespace OctopusCore\Log;

use Psr\Log\LogLevel;

/**
 * Trait LoggerControllerTrait
 * @package OctopusCore\Log
 */
trait LoggerControllerTrait
{
    use LoggerStorageTrait;

    /**
     * @param string $code
     * @param array $context
     * @param string $logger
     * @return mixed|void
     */
    public function debug(
        string $code,
        array $context = array(),
        string $logger = '0'
    ) {
        $this->log(LogLevel::DEBUG, $code, $context, $logger);
    }

    /**
     * @param $level
     * @param string $code
     * @param array $context
     * @param string $logger
     * @return mixed
     */
    abstract public function log(
        $level,
        string $code,
        array $context = array(),
        string $logger = '0'
    );
}