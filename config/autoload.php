<?php

const AUTOLOAD_ALIASES = [
    "OctopusCore" => "src/OctopusCore"
];

/**
 * @param $class_name
 */
function autoload($class_name): void
{
    $class_array = explode("\\", $class_name);
    $array_count = count($class_array);
    if ($array_count < 2) {
        die("Bad namespace declaration");
    }

    foreach ($class_array as $value) {
        if (!preg_match('~^\p{Lu}~u', $value)) {
            die("Bad namespace declaration");
        }
    }

    if (!array_key_exists($class_array[0], AUTOLOAD_ALIASES)) {
        array_unshift($class_array, "vendor");
    } else {
        $class_array[0] = AUTOLOAD_ALIASES[$class_array[0]];
    }

    $class_file = implode('/', $class_array) . '.php';
    $class_path = realpath(__DIR__ . '/..') . '/' . $class_file;

    require_once $class_path;
}

spl_autoload_register('autoload');