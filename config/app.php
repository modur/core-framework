<?php

declare(strict_types=1);
header("Content-Type: text/plain");

define("DIR_ROOT", realpath(__DIR__ . "/.."));
define("DIR_BIN", DIR_ROOT . "/bin/");
define("DIR_CONF", DIR_ROOT . "/config/");
define("DIR_DOCS", DIR_ROOT . "/docs/");
define("DIR_LANG", DIR_ROOT . "/locales/");
define("DIR_LOG", DIR_ROOT . "/logs/");
define("DIR_PUBLIC", DIR_ROOT . "/public/");
define("DIR_RES", DIR_ROOT . "/resources/");
define("DIR_ROUTE", DIR_ROOT . "/routes/");
define("DIR_SRC", DIR_ROOT . "/src/");
define("DIR_TEST", DIR_ROOT . "/tests/");
define("DIR_VENDOR", DIR_ROOT . "/vendor/");