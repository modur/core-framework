# Modur Framework
See [CHANGELOG](https://gitlab.com/modur/one/core-framework/-/blob/main/CHANGELOG.md) for changes.
## About
Modur Framework is the core PHP framework of platform Modur One which is currently in development. This framework is focused mainly on web applications development. However you can use it on your own.
## Documentation
You can find our documentation [here](https://gitlab.com/modur/one/core-framework/-/blob/main/docs/OctopusDoc/INDEX.md).
## Contributing
Are you interested in contribute to our framework? Visit [this](https://gitlab.com/modur/one/core-framework/-/blob/main/CONTRIBUTING.md) page.
## License
The Modur Framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
## About us
If you want to know something about our team, please visit our websites [modur.one](https://modur.one).

###### tags: `Modur Framework v1.0.0`
