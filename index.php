<?php

use OctopusCore\App\App;

$init_conf = realpath(__DIR__) . "/config/init.php";
if (!file_exists($init_conf)) {
	require_once realpath(__DIR__) . '/README.html';
	die();
}

require_once $init_conf;


/** @var App $app */
$app->start();